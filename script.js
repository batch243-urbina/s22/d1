console.log(`Tulog paaaaa`);

// Built-in methods and functions for arrays

let fruits = ["Apple", "Orange", "Kiwi", "Banana", "Grapes"];
console.log(fruits);

// Mutator Methods - change an array after they are created
// PUSH Method - add last element in an array and returns the length
// arrayName.push("elementToBeAdded")
console.log(`PUSH`);
console.log(`Current Array Fruits[]`);
console.log(fruits);

let addedFruit = fruits.push();
console.log(`Mutated Array Fruits[]`);
fruits.push("Mango");
console.log(fruits);
console.log(addedFruit);

// Adding multiple elements to an array
fruits.push("Avocado", "Guava");
console.log(`Mutated Array Fruits[]`);
console.log(fruits);

// POP Method - removes last element in array
// arrayName.pop();
console.log(`POP`);
let removedFruit = fruits.pop();
console.log(fruits);
console.log(removedFruit);

removedFruit = fruits.pop();
console.log(`Mutated Array Fruits[]`);
console.log(fruits);
console.log(removedFruit);

// UNSHIFT - add one or more elements at beginning of an array
console.log(`UNSHIFT`);
console.log(`Current Array Fruits: `);
console.log(fruits);

let addedFruitsAtStart = fruits.unshift();
fruits.unshift("Lime", "Lanzones");
console.log(`Mutated Array Fruits[]`);
console.log(fruits);
console.log(addedFruitsAtStart);

// SHIFT - removes element at beginning of an array
console.log(`SHIFT`);
console.log(`Current Array Fruits: `);
console.log(fruits);

console.log(`Mutated Array Fruits[]`);
fruits.shift();
console.log(fruits);

// SPLICE - removes elements simultaneously from a specified index number and adds elements
// arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
console.log(`SPLICE`);
console.log(`Current Array Fruits: `);
console.log(fruits);

console.log(`Mutated Array Fruits[]`);
fruits.splice(1, 0, "Lime", "Rambutan", "Atis");
console.log(fruits);

// SORT - rearranges the array elements in alphanumeric order
// arrayName.sort();

console.log(`SORT`);
console.log(`Current Array Fruits: `);
console.log(fruits);

console.log(`Mutated Array Fruits[]`);
fruits.sort();
console.log(fruits);

// REVERSE - reverses the order
console.log(`REVERSE`);
console.log(`Current Array Fruits: `);
console.log(fruits);

console.log(`Mutated Array Fruits[]`);
fruits.reverse();
console.log(fruits);

// NON MUTATOR METHODS - functions that do not modify or change an array after they are created
// these methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DEN"];

// indexOf() - returns the index number of the first matching element in an array
// if no match found, -1 will be returned
// search process beginning to last
// arrayName.indexOf(searchValue);
// arrayName.indexOf(searchValue, startingIndex);

console.log(countries.indexOf("DEN"));
console.log(countries.indexOf("PH", 2));

// lastIndexOf();
// returns index number of last matching element
// search process from last to beginning

console.log(countries.lastIndexOf("PH"));

// SLICE - portion/slices from an array and returns a new array
// arrayName.slice(startingIndex);
// arrayName.slice(startingIndex, endingIndex);

let slicedArray = countries.slice(2);
console.log(slicedArray);

slicedArray = countries.slice(0, 5);
console.log(slicedArray);

slicedArray = countries.slice(-2);
console.log(slicedArray);

// TO STRING - returns an array as a string separated by commas
// arrayName.toString().

let stringedArray = countries.toString();
console.log(stringedArray);

// CONCAT - combines multiple arrays and returns the combined result
// arrayA.concat(arrayB);
// arrayA.concat(elementA)

console.log(fruits.concat(countries));
console.log(fruits.concat(slicedArray));

let tasks = [
  "drink HTML",
  "eat javascript",
  "breathe SASS",
  "get git",
  "be node",
];
console.log(fruits.concat(countries, tasks));

// JOIN - returns an array as a string separated by specified separator string
// arrayName.join("separatorString");

let users = ["John", "Jane", "Joe", "Robert"];
console.log(users.join(", "));

// ITERATION METHODS
// loop designed to perform repetitive tasks on arrays
// loops over all elements in array

// FOR EACH
// similar to a for loop that iterates on each of array element
// for each element in the array, the function in the for each method will be run
// arrayName.forEach(function(individualElement){
// 	statements;
// })

console.log(tasks);
tasks.forEach(function (task) {
  console.log(task);
});

let filteredTasks = [];
tasks.forEach(function (task) {
  if (task.length > 10) {
    filteredTasks.push(task);
  }
});
console.log(filteredTasks);

// MAP - iterates on each element and returns new array with different values depending on the result of the function's operation
// let/const resultArray = arrayName.map(function(elements)) {
// 	statements;
// 	return;
// })

let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
let numberMap = numbers.map(function (number) {
  return number * number;
});
console.log(numberMap);

// EVERY - checks wether all elements pass the condition
// data validation, when dealing large amounts of data
// let/const resultArray = arrayName.every(function(element) {
// 	return expression;
// })

console.log(numbers);
let allValid = numbers.every(function (number) {
  return number < 10;
});
console.log(allValid);

// SOME - checks if at least one element in the array meets the given condition;
// returns a true value if at least 1 element meets the condition
// let/const resultArray = arrayName.some(function(elements) {
// 	return expression;
// })

console.log(numbers);
let someValid = numbers.some(function (number) {
  return number === 10;
});
console.log(someValid);

// FILTER - return new array that contains the elements which meets the condition
// let/const resultArray = arrayName.filter(function(element) [
// 	return XPathExpression;
// ])

console.log(numbers);
let filterValid = numbers.filter(function (number) {
  return number % 2 === 0;
});
console.log(filterValid);

// INCLUDES - checks if the argument passed can be found in array
// arrayName.includes("element");

console.log(numbers.includes(5));
console.log(numbers.includes(25));

// REDUCE - evaluates elements from left to right, return/reduces the array into a single value
// // let/const resultValue = arrayName.reduce(function(accumulator, currentValue) {
// 	return statement/expression;
// })

let numbersReduce = [1, 2, 3, 4, 5, 6, 7, 8, 9];
console.log(numbersReduce);

let total = numbersReduce.reduce(function (x, y) {
  console.log(`This is the value of x: ${x}`);
  console.log(`This is the value of y: ${y}`);
  return x + y;
});
console.log(total);
